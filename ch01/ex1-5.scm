;; If the interpreter uses applicative-order evaluation, the evaluation steps are:
;; (test 0 (p))
;; (test 0 (p))
;; (test 0 (p))
;; ...
;; because the interpreter evaluates the operands before the operator, so (p) is 
;; called over and over again.


;; If the interpreter uses normal-order evaluation, the evaluation steps are:
;; (test 0 (p))
;; 0
;; 
;; because the operands are not evaluated until they are needed, therefore the 
;; first argument '0' is evaluated, which causes the procedure to return zero. 
;; The second argument (p) is never needed, thus never evaluated.