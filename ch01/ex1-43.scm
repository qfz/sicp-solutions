(define (compose f g)
  (lambda (x) (f (g (x)))))

(define (repeat f n)
  (lambda (x) (compose (repeat f (- n 1)) f)))
