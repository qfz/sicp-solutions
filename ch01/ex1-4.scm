(a-plus-abs-b a b)

;;; a plus the abs value of b
;;; if b is larger than 0, the + operator is used to sum a and b.
;;; if b is not larger than 0, the - operator is used to substract b from a.
;;; This procedure only gets the absolute value of b; a retains its original va-
;;; lue.
