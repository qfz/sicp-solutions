;;; A procedure that takes three numbers as arguments and returns the sum of the
;;; tow larger numbers.

;;; Tried to compute the smallest number and add the square of other two, didn't
;;; work.

(define (sum-of-sqr-of-largest-two a b c)
  (cond ((and (< a b) (< a c)) (+ (square b) (square c)))
        ((and (< b a) (< b c)) (+ (square a) (square c)))
        ((and (< c a) (< c b)) (+ (square a) (square b)))))

(define (square x)
  (* x x))