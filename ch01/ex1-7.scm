;; For very small numbers like 0.0001 the good-enough? procedure will always 
;; return true because the difference between very small numbers is always very 
;; small, thus the original version of good-enough? is not good enough.

;; For very large numbers, if the precision is limited and larger than 0.001, 
;; then it's possible that good-enough? will always return false because the 
;; difference between two very large number might be always larger than 0.001.

#lang racket
(define (sqrt2 x)
  (sqrt-iter 1.0 x))

(define (sqrt-iter guess x)
  (if (good-enough? guess (improve guess x))
      (improve guess x)
      (sqrt-iter (improve guess x)
                 x)))

(define (good-enough? guess next-guess)
  (< (abs (- guess next-guess)) 0.001))

(define (improve guess x)
  (average guess (/ x guess)))

(define (average a b)
  (/ (+ a b) 2))

; Test
(sqrt2 0.0001)