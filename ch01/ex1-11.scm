;;; Recursive version
(define (f n)
  (if (< n 3)
      n
      (+ (f (- n 1)) (* 2 (f (- n 2))) (* 3 (f (- n 3))))))

;;; Iterative version
(define (f2 n)
  (f-iter 0 1 2 n))

(define (f-iter a b c count)
  (if (= count 0)
      a
      (f-iter b
              c
              (+ c (* 2 b) (* 3 a)) 
              (- count 1))))
