
(define (f g)
  (g 2))

(define square
  (lambda (x) (* x x)))

(f square)

(f (lambda (z) (* z (+ z 1))))

(f f)

;;; If we evaluate (f f), the interpreter gives erro message saying prcedure is 
;;; required, but was given the number '2'.

;;; What happens is that f is passed to f as argument, the the body (f 2) is 
;;; evaluated. However, (f 2) is invalid because f expects a procedure for its 
;;; argument, not a number.