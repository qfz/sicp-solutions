The results printed by the interpreter:

10	; The value of 10 is 10.

12	; The sum of 5, 3 and 4 is 12.

8	; 9 minus 1 is 8.

3	; 6 divided by 2 is 3.

6	; The product of 2 and 4 is 8, 4 minus 6 is -2, the sum of 8 and -2 is 6

a	; a is defined, its value is 3.

b	; b is defined, its value is a + 1, which is 4.

19	; a is 3, b is 4, the product of a and b is 12, the sum of 3 and 4 and 
	; 12 is 19.

#f	; false is returned, a is not equal to b.

4	; If both predicates are true, return b, otherwise return a.

16	; If a is equal to 4, return 6.
	; If b is equal to 4, return the sum of 6, 7 and a(3).
	; Else return 25.
	; In this case, the sum of 6, 7 and a(3) is returned.

16	; First, evaluate the cond, the second predicate is true, so b(4) is re-
	; turned.
	; Then, the product of 4 and (+ a 1) is 16 because a = 3.
