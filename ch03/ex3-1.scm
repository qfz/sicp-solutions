(define (make-accumulator sum)
  (lambda (a)
    (set! sum (+ sum a))
    sum))
