#lang racket

;; Possibilities 101, 121, 110 and 100 will remain. Because only the execution 
;; part of p1 is serialized, so x can still be accessed concurrently, just 
;; can't be modified.