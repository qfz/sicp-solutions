
(define (make-monitored f)
  (let ((counter 0))
    (define (mf m)
      (cond ((eq? m 'how-many-calls?) counter)
            ((eq? m 'reset-count) reset-count)
            (else (begin (set! counter (+ counter 1))
                         (f m)))))
    (define reset-counter
      (set! counter 0))
    mf))

