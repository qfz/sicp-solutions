(equal? '(this is a list) '(this is a list))

(equal? '(this is a list) '(this (is a) list))

(define (equal? a b)
  (cond ((and (not (pair? a)) (not (pair? b)) (eq? a b)) 
         true)
        ((and (pair? a) 
              (pair? b) 
              (eq? (car b) (car b))
              (not (null? (cdr a)))
              (not (null? (cdr b))))
         (equal? (cdr a) (cdr b)))
        (else false)))
