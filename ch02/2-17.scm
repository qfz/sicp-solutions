#lang racket
; last-pair: returns the list that contains only the last element of a given list
(define (last-pair items)
  (if (null? (cdr items))
      (car items)
      (last-pair (cdr items))))
