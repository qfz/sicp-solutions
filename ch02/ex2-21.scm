;;; Received error: "reference to undefined identifier: nil"
;;; Doesn't racket define "nil"?
(define (square-lite items)
  (if (null? items)
      nil
      (cons (square (car items)) (square-lite (cdr items)))))

(define (square-lite2 items)
  (map (lambda (x) (square x)) items))

(define (square x)
  (* x x ))
