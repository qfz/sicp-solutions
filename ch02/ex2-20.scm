(define (same-parity first . rest)
  (cond (odd? (first)) (get-odd rest)
        (even? (first)) (get-even rest)))

(define (get-odd items)
  (if (odd? (car items)) (car items)
      (get-odd (cdr items))))

(define (get-even items)
  (if (even? (car items)) (car items)
      (get-even (cdr items))))
