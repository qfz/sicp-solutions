; The first procedure doesn't work because it takes the square of the first
; element of cdr and cons it to the previous answer. The previous answer 
; should be before the first element of cdr, but now is after.
;
; For example, consider (iter (list 1 2 3) nil):
; (iter (list 1 2 3) nil)
; (iter (list 2 3) (list 1 nil)
; (iter (list 3) (list 4 1 nil))
; (iter nil (list 9 4 1 nil))

; The second procedure doesn't work because the parameter "answer" is a 
; list, so the result becomes (list (list (list ...)))
