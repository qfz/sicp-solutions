(define (cons x y)
  (lambda (m) (m x y)))

(define (car z)
  (z (lambda (p q) p)))

; Verification of (car z)
; (car z)
; (car (cons x y))
; (lambda (lambda (p q) p))
; (lambda (p q) x y)
; (x)

; Definition of cdr
(define (cdr z)
  (z (lambda (p q) q)))

