#lang racket
; reverse-2: takes a list as argument and returns a list of the same elements in reverse order.
; It's name reverse-2 to distinguish it from the 'reverse' procedure that comes with racket. 

(define (reverse-2 items)
  (if (null? items)
      '()
      (append (reverse-2 (cdr items)) (list (car items)))))

(reverse-2 (list 1 4 9 16 25))


