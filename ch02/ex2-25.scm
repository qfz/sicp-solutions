; The result produced by this expression is '(7), still a list, why?
(cdr (car (cdr (cdr (list 1 3 (list 5 7) 9)))))

; The result produced by this expression is 7, not a list.
(car (car (list (list 7))))

; I tried this, but wrong. 
(cdr (cdr (cdr (cdr (cdr (cdr (list 1 (list 2 (list 3 (list 4 (list 5 (list (6 7)))))))))))))
