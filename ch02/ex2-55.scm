(car ''abracadabra)
; => 'quote

; The interpreter returs 'quote because the object quoted is 'abracadabra,
; which is a quote itself, not a symbol.
