(define x (list 1 2 3))
(define y (list 4 5 6))


; result of (append x y): '(1 2 3 4 5 6)

; Don't fully understand this one
; result of (cons x y): '((1 2 3) 4 5 6))

; result of (list x y): '((1 2 3) (4 5 6))
