(define (for-each proc items)
  (proc (car items))
  (for-each proc (cdr items)))
